﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace __Template__.Cmd.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SamplesController 
        : ControllerBase
    {
        [HttpGet]
        public IEnumerable<string> Get()
        {
            var samples =
                new List<string>
                {
                    "sample1", "sample2", "sample3"
                };

            return samples;
        }
    }
}
