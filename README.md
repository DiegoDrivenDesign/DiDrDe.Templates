# DiDrDe.Templates

## Install
To install the template run
`dotnet new --install D:\foo\bar\src\DiDrDe.Templates\core3ddd`
which will install the `.template/config/template.json` specified at the path of the specific template

## Uninstall 
To uninstall the template run
`dotnet new -u D:\foo\bar\src\DiDrDe.Templates\core3ddd`
which will uninstall the template defined that the provided absolute path

## Use
Once the template is installed for the current dotnet framework, use it to create the
desired projects by running:
`dotnet new core3ddd -o Foo.Bar`